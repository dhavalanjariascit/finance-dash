import logging
import config
from flask import Blueprint, render_template
import os
import pandas as pd
import pathlib
import config
import json
import plotly

log = logging.getLogger('app')

blueprint = Blueprint("api", __name__)


@blueprint.route("/data/<company>", methods=['GET'])
def get_page_for_company(company):
	earnings_json = get_earnings_graph(company)

	return jsonify(earings=earnings_json)


def get_company_info(company):

	folder = path = pathlib.Path(os.getcwd())
	print(folder)
	folder = folder.joinpath('data/' + company).joinpath('about.txt')
	
	info = ''
	with folder.open() as f:
		info = f.read()

	return info

	



def get_earnings_quarterly(company):
	
	folder = path = pathlib.Path(os.getcwd())
	print(folder)
	folder = folder.joinpath('data/' + company)


	earnings_csv = folder.joinpath('earnings.csv')

	print(str(earnings_csv))
	earnings_df = pd.read_csv(str(earnings_csv), encoding='utf-8')

	# Check data frame for col and row values
	quaterly_earnings = earnings_df.iloc[1:4, 1:3]
	yearly_earnings = earnings_df.iloc[1:4, 3:5]

	quaterly_values = [earnings_df.iloc[x][0] for x in range(1, 3)]

	quaterly_average = dict(
		x=quaterly_earnings.columns.to_list(),
		y=[x for x in quaterly_earnings.loc[1]],
		type='bar',
		name='Average'
	)

	quaterly_low = dict(
		x=quaterly_earnings.columns.to_list(),
		y=[x for x in quaterly_earnings.loc[2]],
		type='bar',
		name='Low'
	)
	
	quaterly_high = dict(
		x=quaterly_earnings.columns.to_list(),
		y=[x for x in quaterly_earnings.loc[3]],
		type='bar',
		name='High'
	)

	layout = dict(barmode='group')

	graph = dict(
		trace1=quaterly_low,
		trace2=quaterly_average,
		trace3=quaterly_high,
		layout=layout
	)

	graphJSON = json.dumps(graph, cls=plotly.utils.PlotlyJSONEncoder)
	return graphJSON




def get_earnings_yearly(company):
	
	folder = path = pathlib.Path(os.getcwd())
	print(folder)
	folder = folder.joinpath('data/' + company)


	earnings_csv = folder.joinpath('earnings.csv')

	print(str(earnings_csv))
	earnings_df = pd.read_csv(str(earnings_csv), encoding='utf-8')

	# Check data frame for col and row values

	yearly_earnings = earnings_df.iloc[1:4, 3:5]

	yearly_average = dict(
		x=yearly_earnings.columns.to_list(),
		y=[x for x in yearly_earnings.loc[1]],
		type='bar',
		name='Average'
	)

	yearly_low = dict(
		x=yearly_earnings.columns.to_list(),
		y=[x for x in yearly_earnings.loc[2]],
		type='bar',
		name='Low'
	)
	
	yearly_high = dict(
		x=yearly_earnings.columns.to_list(),
		y=[x for x in yearly_earnings.loc[3]],
		type='bar',
		name='High'
	)

	layout = dict(barmode='group')

	graph = dict(
		trace1=yearly_low,
		trace2=yearly_average,
		trace3=yearly_high,
		layout=layout
	)

	graphJSON = json.dumps(graph, cls=plotly.utils.PlotlyJSONEncoder)
	print(graphJSON)
	return graphJSON


def get_revenue_quarterly(company):
	folder = path = pathlib.Path(os.getcwd())
	print(folder)
	folder = folder.joinpath('data/' + company)

	csv = folder.joinpath('revenue.csv')

	print(str(csv))
	df = pd.read_csv(str(csv), encoding='utf-8')

	# Check data frame for col and row values

	df = df.iloc[0:4, 1:3]

	average = dict(
		x=df.columns.to_list(),
		y=[x for x in df.loc[0]],
		type='bar',
		name='Average'
	)

	low = dict(
		x=df.columns.to_list(),
		y=[x for x in df.loc[1]],
		type='bar',
		name='Low'
	)
	
	high = dict(
		x=df.columns.to_list(),
		y=[x for x in df.loc[2]],
		type='bar',
		name='High'
	)

	layout = dict(barmode='group')

	graph = dict(
		trace1=low,
		trace2=average,
		trace3=high,
		layout=layout
	)

	graphJSON = json.dumps(graph, cls=plotly.utils.PlotlyJSONEncoder)
	print(graphJSON)
	return graphJSON


def get_revenue_yearly(company):
	folder = path = pathlib.Path(os.getcwd())
	print(folder)
	folder = folder.joinpath('data/' + company)

	csv = folder.joinpath('revenue.csv')

	print(str(csv))
	df = pd.read_csv(str(csv), encoding='utf-8')

	# Check data frame for col and row values

	df = df.iloc[0:4, 3:5]

	average = dict(
		x=df.columns.to_list(),
		y=[x for x in df.loc[0]],
		type='bar',
		name='Average'
	)

	low = dict(
		x=df.columns.to_list(),
		y=[x for x in df.loc[1]],
		type='bar',
		name='Low'
	)
	
	high = dict(
		x=df.columns.to_list(),
		y=[x for x in df.loc[2]],
		type='bar',
		name='High'
	)

	layout = dict(barmode='group')

	graph = dict(
		trace1=low,
		trace2=average,
		trace3=high,
		layout=layout
	)

	graphJSON = json.dumps(graph, cls=plotly.utils.PlotlyJSONEncoder)
	print(graphJSON)
	return graphJSON
