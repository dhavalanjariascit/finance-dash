import logging
import config
import yfinance as yf
import json
import plotly
import pandas_datareader
yf.pdr_override()

import config
from flask import Blueprint, render_template


log = logging.getLogger('app')

blueprint = Blueprint("api", __name__)


@blueprint.route("/data", methods=['GET'])
def get_data():
    graphJSON = get_plot()
    return graphJSON





def get_candlestick_plot(symbol):
    ticker = yf.Ticker(symbol)
    df = ticker.history(period='2d', interval='15m')

    graphs = [
        dict( # Define trace stuff here
            data=[
                dict(
                    open=df.Open,
                    high=df.High,
                    low=df.Low,
                    close=df.Close,
                    type='candlestick',
                    x=df.index.to_list(),
                    xaxis='x',
                    yaxis='y',
                    decreasing=dict(
                        line=dict(
                            color='#7F7F7F'
                        )
                    ),
                    increasing=dict(
                        line=dict(
                            color='#17BECF'
                        )
                    ),
                    line=dict(
                        color='rgba(31,119,180,1)'
                    )
                )
            ],
            layout=dict(
            )
        )
    ]

    graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)
    return graphJSON


