from logging.config import dictConfig
from collections import namedtuple


data_folder = 'data/'
flask_port = "8000"
flask_host = "0.0.0.0"



earnings_row_indices_tup = namedtuple(
        'earnings_row_indices_tup', ['average', 'low', 'high', 'eps']
    )

earnings_row_indices = earnings_row_indices_tup(
    'Avg. Estimate',
    'Low estimate',
    'High estimate',
    'Year ago EPS'
)


earnings_col_indices_tup = namedtuple(
        'earnings_row_indices_tup', ['current_qtr', 'next_qtr', 'current_year', 'next_year']
    )

earnings_row_indices = earnings_row_indices_tup(
    'Current qtr.(Mar2020)',
    'Next qtr.(Jun2020)',
    'Current year(2020)',
    'Next year(2021)'
)

symbols = {
    'AMZN': 'amazon',
    'WMT': 'walmart',
    'JD': 'jd',
    'BABA': 'alibaba',
    'EBAY': 'ebay'
}


# Logging
LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '[%(filename)s:%(lineno)s - %(funcName)s()] %(message)s',
        },
        'info': {
            'format': '[%(asctime)s]: %(message)s',
            'datefmt': '%H:%M:%S'
        }
    },
    'handlers': {
        'debugfilehandler': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': 'app.log',
            'formatter': 'default'
        },
        'consolehandler': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'info'
        }
    },
    'loggers': {
        'app': {
            'handlers': ['debugfilehandler', 'consolehandler'],
            'level': 'DEBUG',
            'propogate': True,
        },
        'console': {
            'handlers': ['consolehandler', 'debugfilehandler'],
            'level': 'INFO',
            'propogate': True
        }
    }
}

dictConfig(LOGGING_CONFIG)
