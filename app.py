from flask import Flask, render_template, send_from_directory, Blueprint

import json
import yfinance as yf
import pandas_datareader as pdr
yf.pdr_override()
import pandas as pd
import numpy as np
from flask_cors import CORS
import os
import views
from api import data
import logging
log = logging.getLogger('app')

app = Flask(__name__, static_folder=os.path.abspath('/static'), static_url_path='', template_folder='../templates')
app.debug = True
CORS(app)

def create_app():
	with app.app_context():
		app.register_blueprint(views.blueprint)
		app.register_blueprint(data.blueprint, url_prefix='/api/')

# @app.route("/")
# def index():
# 	ticker = yf.Ticker('TATAMOTORS.NS')
# 	df = ticker.history(period='2d', interval='15m')

# 	graphs = [
# 		dict( # Define trace stuff here
# 			data=[
# 				dict(
# 					open=df.Open,
# 					high=df.High,
# 					low=df.Low,
# 					close=df.Close,
# 					type='candlestick',
# 					x=df.index.to_list(),
# 					xaxis='x',
# 					yaxis='y',
# 					decreasing=dict(
# 						line=dict(
# 							color='#7F7F7F'
# 						)
# 					),
# 					increasing=dict(
# 						line=dict(
# 							color='#17BECF'
# 						)
# 					),
# 					line=dict(
# 						color='rgba(31,119,180,1)'
# 					)
# 				)
# 			],
# 			layout=dict(

# 			)
# 		)
# 	]

# 	# Add 'ids' to each of the graphs to pass up to the client
# 	# for templating
# 	ids = ['graph-{}'.format(i) for i, _ in enumerate(graphs)]
# 	# Convert the figures to JSON using PlotlyJSONEncoder
# 	graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)

# 	return render_template('index.html', ids=ids, graphJSON=graphJSON)


if __name__ == '__main__':
	create_app()
	app.run(debug=True, host='0.0.0.0', port=8000)