from flask import (
    current_app as app,
    render_template,
    request,
    redirect,
    abort,
    url_for,
    session,
    Blueprint,
    Response,
    send_file,
)
from api import company_graphs


blueprint = Blueprint("views", __name__, template_folder="templates",
                  static_folder="static",
                  static_url_path="/static/")


@blueprint.route("/", defaults={"route": "index"})
@blueprint.route("/<path:route>")
def static_html(route):
    """
    Basic route
    :param route:
    :return:
    """

    return render_template('index.html')


@blueprint.route("/company/<company_name>", methods=['GET'])
def get_amazon_page(company_name):
    return render_template(company_name + '.html')


@blueprint.route("/sector", methods=['GET'])
def get_sector():
    return render_template('sector.html')



@blueprint.route("/predictions", methods=['GET'])
def get_preds():
    return render_template('predictions.html')

# @blueprint.route("/company/<company>", methods=['GET'])
# def render_company(company):

#     info = company_graphs.get_company_info(company)

#     earnings_quarterly = company_graphs.get_earnings_quarterly(company)
#     earnings_yearly = company_graphs.get_earnings_yearly(company)
#     revenue_quarterly = company_graphs.get_revenue_quarterly(company)
#     revenue_yearly = company_graphs.get_revenue_yearly(company)



#     return render_template('company.html',
#         info=info,
#         earnings_quarterly=earnings_quarterly,
#         earnings_yearly=earnings_yearly,
#         revenue_quarterly=revenue_quarterly,
#         revenue_yearly=revenue_yearly)
#     