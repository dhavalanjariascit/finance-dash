$(
	$.ajax({
		url: "/data",
		type: "GET",
		contentType: 'application/json;charset=UTF-8',
		dataType: "json",
		success: function(res) {
			Plotly.newPlot("candelstick", res);
		}

	})
)